import React, { FC } from "react";
import { Switch, Route } from "react-router-dom";

import Rating from "../Pages/Rating";
import Type from "../Pages/Type";
import Details from "../Pages/Details";

const Routes: FC = () => (
  <Switch>
    <Route path="/" exact component={Type} />
    <Route path="/rating" exact component={Rating} />
    <Route path="/details" exact component={Details} />
  </Switch>
);

export default Routes;
