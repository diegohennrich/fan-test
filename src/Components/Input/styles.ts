import styled, { css } from "styled-components";
import Tooltip from "../Tooltip";

interface ContainerProps {
  isFilled: boolean;
  isFocused: boolean;
  isError: boolean;
  bgColor?: string;
  borderColor?: string;
  color?: string;
  small?: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: #ffffff;
  border-radius: 3px;
  padding: ${(props) => (props.small ? "5px" : "16px")};

  border: solid 1px;
  border-color: ${(props) =>
    props.borderColor ? props.borderColor : "#232129"};
  color: #666360;

  display: flex;
  align-items: center;

  input {
    flex: 1;
    color: black;
    background: transparent;
    border: 0 !important;
    outline: none !important;

    & ::placeholder {
      color: #666360;
    }
  }

  svg {
    margin-right: 15px;
  }

  & ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  & ::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Legend = styled.span`
  font-family: Helvetica Neue;
  color: #999999;
  margin: auto;
  margin-top: 5px;
`;
