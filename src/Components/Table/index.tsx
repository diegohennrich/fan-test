import React, { FC, useCallback, useState } from "react";
import { Container, Header, Title, BoxTable, BoxSubtitle } from "./styles";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Checkbox } from "@material-ui/core";

interface Props {
  title?: string;
  handleClick: () => void;
  data: Array<any>;
  headers: Array<string>;
  styleTitle?: object;
  styleContainer?: object;
  subtitle?: string;
}

const TableData: FC<Props> = ({
  title,
  styleTitle = {},
  handleClick,
  data,
  headers,
  styleContainer = {},
  subtitle,
}) => {
  const [checked, setChecked] = useState(0);

  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });

  const classes = useStyles();

  const handleChange = useCallback((event: number) => {
    console.log("event: ", event);
    setChecked(event);
    handleClick();
  }, []);

  return (
    <Container style={styleContainer}>
      <Header>
        <Title style={styleTitle}>{title}</Title>
      </Header>
      <BoxTable>
        <TableContainer>
          <BoxSubtitle>
            <span>{subtitle}</span>
          </BoxSubtitle>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left"></TableCell>

                {headers.map((i) => (
                  <TableCell key={i} align="center">
                    {i}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  {Object.entries(row).map((value: any, key: any) => {
                    if (value[0] === "id") return;

                    if (value[0] === "type" && value[1] === "checkbox") {
                      return (
                        <TableCell align="right">
                          <Checkbox
                            checked={checked === row.id}
                            onChange={() => handleChange(row.id)}
                            inputProps={{ "aria-label": "primary checkbox" }}
                          />
                        </TableCell>
                      );
                    } else {
                      return <TableCell align="center">{value[1]}</TableCell>;
                    }
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </BoxTable>
    </Container>
  );
};

export default TableData;
