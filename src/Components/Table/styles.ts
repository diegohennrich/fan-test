import styled from "styled-components";

export const Container = styled.div`
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  border: 0;
  display: flex;
  position: relative;
  font-size: 0.875rem;
  min-width: 0;
  word-wrap: break-word;
  background: #fff;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
  margin-top: 30px;
  border-radius: 6px;
  margin-bottom: 30px;
  flex-direction: column;

  .MuiTableCell-head {
    color: #ff9800;
  }
`;

export const Header = styled.div`
  height: 84px;
  margin: 0 20px;
  margin-top: -30px;
  border-radius: 3px;
  position: relative;
  background: linear-gradient(60deg, #ffa726, #fb8c00);
  box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.14),
    0 7px 10px -5px rgba(255, 152, 0, 0.4);
  display: flex;
  align-items: center;
`;

export const Title = styled.span`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  color: #ffffff;
  margin-left: 20px;

  /* margin-top: 0px;
  min-height: auto;

  font-weight: 300;
  margin-bottom: 3px;
  text-decoration: none; */
`;

export const BoxTable = styled.div`
  margin: 0 20px 20px 20px;
`;

export const BoxSubtitle = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin-top: 30px;
  margin-bottom: 10px;

  span {
    margin: auto;
    color: #ff9800;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  }
`;
