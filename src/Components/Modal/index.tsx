import React, {
  RefForwardingComponent,
  forwardRef,
  useState,
  useCallback,
  useImperativeHandle,
} from "react";

export interface HandleModal {
  handleVisible: () => void;
}
const InputTest: RefForwardingComponent<HandleModal> = (_, ref) => {
  const [visible, setVisible] = useState(false);

  const handleVisible = useCallback(() => {
    setVisible((prev) => !prev);
  }, []);

  useImperativeHandle(ref, () => {
    return {
      handleVisible,
    };
  });
  return (
    <div className="container">{visible && <span>Está visível</span>}</div>
  );
};

export default forwardRef(InputTest);
