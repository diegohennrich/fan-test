import styled from "styled-components";

interface ContainerProps {
  justImage?: boolean;
}
export const Container = styled.div<ContainerProps>`
  color: rgba(0, 0, 0, 0.87);
  max-width: ${(props) => (props.justImage ? "200px" : "300px")};
  width: 100%;
  border: 0;
  display: flex;
  position: relative;
  font-size: 0.875rem;
  min-width: 0;
  word-wrap: break-word;
  background: #fff;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
  margin-top: ${(props) => (props.justImage ? "0px" : "30px")};
  border-radius: 6px;
  /* margin-bottom: 30px; */
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-height: 510px;

  button {
    margin-top: 20px;
    background: #0475b9;
    color: white;
    width: 90%;
    border-radius: 30px;
    transition: all 300ms linear;
    display: flex;
    margin-bottom: 30px;
    justify-content: center;
    box-shadow: 0 2px 2px 0 rgba(233, 30, 99, 0.14),
      0 3px 1px -2px rgba(233, 30, 99, 0.2), 0 1px 5px 0 rgba(233, 30, 99, 0.12);

    &:hover {
      background: #0475b9;
    }

    span {
      color: #fff;

      font-size: 14px;
      font-family: "Roboto", "Helvetica", "Arial", sans-serif;
      font-weight: 300;
      line-height: 30px;
    }
  }
`;

export const Content = styled.div`
  padding: 15px !important;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

interface PhotoProps {
  justImage?: boolean;
}
export const Photo = styled.img<PhotoProps>`
  height: ${(props) => (props.justImage ? "150px" : "200px")};
  margin-top: 15px;
  margin-bottom: 15px;
`;

export const Description = styled.div`
  margin: 15px 0px;
  text-align: center;
`;

export const Title = styled.h1`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
`;

interface InfoProps {
  justImage?: boolean;
}

export const Info = styled.div<InfoProps>`
  height: ${(props) => (props.justImage ? "150px" : "280px")};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
