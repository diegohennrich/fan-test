import React, { FC, useCallback } from "react";
import { useHistory } from "react-router-dom";
import { Container, Photo, Description, Content, Title, Info } from "./styles";
import { Button } from "@material-ui/core";
interface Props {
  title?: string;
  image?: any;
  description?: string;
  justImage?: boolean;
  styleContainer?: object;
  styleImage?: object;
  styleInfo?: object;
  styleTitle?: object;
  styleDescription?: object;
  styleBtn?: object;
}

const Card: FC<Props> = ({
  title,
  image,
  description,
  justImage = false,
  styleContainer = {},
  styleImage = {},
  styleInfo = {},
  styleTitle = {},
  styleDescription = {},
  styleBtn = {},
}) => {
  const history = useHistory();

  const handleClick = useCallback(() => {
    history.push("/rating?menu=2");
  }, []);
  return (
    <Container justImage={justImage} style={styleContainer}>
      <Content>
        {title && <Title style={styleTitle}>{title}</Title>}

        <Info justImage={justImage} style={styleInfo}>
          <Photo src={image} justImage={justImage} style={styleImage} />
          {!justImage && (
            <Description style={styleDescription}>{description}</Description>
          )}
        </Info>
        {!justImage && (
          <Button style={styleBtn} className="button" onClick={handleClick}>
            CHOOSE
          </Button>
        )}
      </Content>
    </Container>
  );
};

export default Card;
