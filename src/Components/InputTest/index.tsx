import React, {
  RefForwardingComponent,
  forwardRef,
  InputHTMLAttributes,
} from "react";

interface InputTestProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

const InputTest: RefForwardingComponent<HTMLInputElement, InputTestProps> = (
  { label, ...rest },
  ref
) => {
  return (
    <div className="container">
      <label htmlFor={name}>{label}</label>
      <input type="text" {...rest} ref={ref} />
    </div>
  );
};

export default forwardRef(InputTest);
