import React, { FC } from "react";
import { Container } from "./styles";
import { Line } from "react-chartjs-2";

const Chart: FC = () => {
  const data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        label: "My First dataset",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(255,165,0)",
        borderColor: "rgba(255,165,0)",
        borderCapStyle: "butt",
        // borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(255,165,0)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(255,165,0)",
        pointHoverBorderColor: "rgba(255,165,0)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [65, 59, 80, 81, 56, 55, 40],
      },
      {
        label: "My seconde dataset",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgb(255,0,0)",
        borderColor: "rgb(255,0,0)",
        borderCapStyle: "butt",
        // borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(255,0,0)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(255,0,0)",
        pointHoverBorderColor: "rgb(255,0,0)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [45, 70, 55, 40, 70, 40, 90],
      },
      {
        label: "My third dataset",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgb(0,0,255)",
        borderColor: "rgb(0,0,255)",
        borderCapStyle: "butt",
        // borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(0,0,255)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0,0,255)",
        pointHoverBorderColor: "rgb(0,0,255)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [80, 75, 65, 75, 50, 80, 70],
      },
    ],
  };
  return (
    <Container>
      <Line width={700} height={300} data={data} />
    </Container>
  );
};

export default Chart;
