import React, { FC, useState, useCallback } from "react";
import {
  Container,
  Content,
  Box,
  Header,
  Title,
  Logo,
  Description,
  BoxLogo,
} from "./styles";
import Card from "../Card";
import { Drawer, Button } from "@material-ui/core";
import LogoBig from "../../Assets/logo-maior.jpg";

interface Props {
  visible: boolean;
  handleClose: () => void;
}
const Draw: FC<Props> = ({ visible, handleClose }) => {
  // const handleClose = useCallback(() => {
  //   setOpen((prev) => !prev);
  // }, []);
  return (
    <Container>
      <Drawer anchor="right" open={visible} onClose={handleClose}>
        <Content>
          <Box>
            <Header>
              <Title>INFORMATION</Title>
            </Header>

            <BoxLogo>
              <Logo src={LogoBig} />
            </BoxLogo>
            <Description>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Description>
            <Description>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Description>
          </Box>
        </Content>
      </Drawer>
    </Container>
  );
};

export default Draw;
