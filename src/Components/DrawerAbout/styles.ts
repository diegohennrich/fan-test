import styled from "styled-components";
import Bg from "../../Assets/bg2.png";

export const Container = styled.div``;

export const Content = styled.div`
  width: 800px;
  background: url(${Bg});
  display: flex;
  position: relative;
  background-position: center;
  height: 100vh;
`;

export const Box = styled.div`
  width: 800px;
  position: relative;
  background-color: rgba(0, 0, 0, 0.9);
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  width: 90%;
  border-bottom: solid 1px;
  border-color: #ffffff;
  margin-top: 20px;
  padding-bottom: 10px;
`;

export const Title = styled.span`
  color: #ffffff;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  margin-bottom: 10px;
  font-weight: 300;
`;

export const Description = styled.span`
  color: #ffffff;
  width: 90%;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  margin: 30px;
  font-size: 14px;
  font-weight: 300;
`;

export const BoxLogo = styled.div`
  background: #ffffff;
  border-radius: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60%;
  margin-top: 50px;
`;

export const Logo = styled.img`
  width: 90%;
`;
