import React, { FC, useState, useCallback } from "react";
import {
  Container,
  Content,
  Box,
  Header,
  Title,
  Info,
  Description,
  Footer,
  BoxDimensions,
  Product,
  InfoDimensions,
  BoxProduct,
  AlignProduct,
} from "./styles";
import Card from "../../Components/Card";
import { Drawer, Button } from "@material-ui/core";
import Modelil from "../../Assets/models/cv.png";
import Chart from "../../Components/Chart";
import DataPower from "../../Pages/Rating/data/soundPower";
import DataPressure from "../../Pages/Rating/data/soundPressure";
import Table from "../Table";
import { AiOutlinePrinter } from "react-icons/ai";

interface Props {
  visible: boolean;
  handleClose: () => void;
}
const Draw: FC<Props> = ({ visible, handleClose }) => {
  // const handleClose = useCallback(() => {
  //   setOpen((prev) => !prev);
  // }, []);
  return (
    <Container>
      <Drawer anchor="right" open={visible} onClose={handleClose}>
        <Content>
          <Box>
            <Header>
              <Title>PRODUCT INFORMATION</Title>
              <Button
                startIcon={<AiOutlinePrinter size={20} color="#FFFFFF" />}
                style={{ color: "#FFFFFF", margin: 0 }}
                onClick={() => {}}
              >
                Print
              </Button>
            </Header>

            <Info>
              <Description>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </Description>
              <BoxDimensions>
                <InfoDimensions>
                  <Title color="#000000" weight="bold">
                    Dimensions: Model IL
                  </Title>

                  <div>
                    <Title color="#000000" weight="bold">
                      Size: 28
                    </Title>
                  </div>
                </InfoDimensions>
                <BoxProduct>
                  <span style={{ marginLeft: "auto", marginRight: 25 }}>
                    38
                  </span>
                  <AlignProduct>
                    <Product src={Modelil} />
                    <span>200</span>
                  </AlignProduct>
                  <span
                    style={{
                      marginLeft: "auto",
                      marginTop: 25,
                      marginRight: 30,
                    }}
                  >
                    60
                  </span>
                </BoxProduct>
              </BoxDimensions>
              {/* <Card image={Modelil} justImage={true} /> */}
            </Info>
            <Header>
              <Title>Curves</Title>
            </Header>
            <Chart />

            <Header>
              <Title>Sound</Title>
            </Header>
            <Table
              styleContainer={{ width: "90%", marginTop: 50 }}
              styleTitle={{ margin: "auto" }}
              headers={DataPower.headers}
              data={DataPower.data}
              title="Sound Power Levels (PWL) in dB RE 1.0E-12 watts"
              subtitle="Octave Center Frequency (Hz)"
              handleClick={() => {}}
            />

            <Table
              styleContainer={{ width: "90%", marginTop: 50 }}
              styleTitle={{ margin: "auto" }}
              headers={DataPressure.headers}
              data={DataPressure.data}
              title="Sound Pressure Levels (SPL) in dB RE 0.0002 microbar"
              subtitle="Octave Center Frequency (Hz)"
              handleClick={() => {}}
            />

            <Footer>
              <Button
                startIcon={<AiOutlinePrinter size={20} color="#FFFFFF" />}
                style={{ color: "#FFFFFF", marginBottom: 30 }}
                onClick={() => {}}
              >
                Print
              </Button>
            </Footer>
          </Box>
        </Content>
      </Drawer>
    </Container>
  );
};

export default Draw;
