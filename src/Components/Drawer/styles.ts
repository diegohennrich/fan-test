import styled from "styled-components";
import Bg from "../../Assets/bg2.png";
import DimensionsBg from "../../Assets/dimensions.png";

export const Container = styled.div``;

export const Content = styled.div`
  width: 800px;
  background: url(${Bg});
  display: flex;
  position: relative;
  background-position: center;
`;

export const Box = styled.div`
  width: 800px;
  position: relative;
  background-color: rgba(0, 0, 0, 0.9);
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  width: 90%;
  border-bottom: solid 1px;
  border-color: #ffffff;
  margin-top: 20px;
  padding-bottom: 10px;
`;

interface TitleProps {
  size?: string;
  color?: string;
  weight?: string;
}

export const Title = styled.span<TitleProps>`
  color: ${(props) => props.color || "#FFFFFF"};
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  margin-bottom: 10px;
  font-weight: ${(props) => props.weight || 300};
  font-size: ${(props) => props.size || "15px"};
`;

export const Info = styled.div`
  width: 90%;
  display: flex;
  margin-top: 20px;
  align-items: flex-start;
  justify-content: space-between;
`;

export const Description = styled.span`
  color: #ffffff;
  width: 400px;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  margin-bottom: 10px;
  font-size: 14px;
  font-weight: 300;
`;

export const Footer = styled.div`
  width: 90%;
  display: flex;
`;

export const BoxDimensions = styled.div`
  width: 300px;

  border-radius: 10px;
  background: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const InfoDimensions = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 10px;

  > div {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const Product = styled.img`
  height: 100px;
  margin-top: 40px;
  margin-left: 20px;
  margin-right: 40px;
`;

export const BoxProduct = styled.div`
  width: 200px;
  height: 230px;
  background: url(${DimensionsBg});
  background-repeat: no-repeat;
  margin-top: 30px;
  margin-right: 80px;
  display: flex;
  flex-direction: column;
`;

export const AlignProduct = styled.div`
  display: flex;
  align-items: center;
  margin-left: 50px;

  span {
    margin-top: 20px;
  }
`;
