import styled from "styled-components";
import Tooltip from "../Tooltip";

interface ContainerProps {
  orientation: string;
}

export const Container = styled.div<ContainerProps>`
  display: flex;
  margin: 0px;

  flex-direction: ${(props) =>
    props.orientation === "vertical" ? "column" : "row"};

  .teste {
    color: #00acc1 !important;
  }

  .MuiButtonBase-root {
    padding: 3px !important;
  }

  .teste:hover {
    color: #00acc1 !important;
  }

  .teste:checked {
    color: #00acc1 !important;
    background-color: #00acc1 !important;
  }

  /* input {
    background: transparent;
    border: 0;
    margin-right: 5px;
  }

  span {
    color: black;
    margin: 0;
    margin-left: 5px;
    min-width: 25px !important;
  } */

  /* label {
    margin: 0 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
  } */
`;
