import React, {
  FC,
  InputHTMLAttributes,
  useRef,
  useEffect,
  useCallback,
  useState,
} from "react";
import { FiAlertCircle } from "react-icons/fi";
import { useField } from "@unform/core";
import { Container } from "./styles";
import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

interface Option {
  id: string | number;
  label: string;
}

interface Props {
  name: string;
  options: Option[];
  clickExtraFunction?: (value?: string) => void;
}

interface InputProps extends InputHTMLAttributes<HTMLInputElement>, Props {
  name: string;
  orientation: "vertical" | "horizontal";
}
const RadioButton: FC<InputProps> = ({
  name,
  options,
  clickExtraFunction,
  orientation,
}) => {
  const [valueInput, setValueInput] = useState(null);
  const inputRef = useRef<HTMLInputElement[]>([]);
  const { fieldName, registerField, defaultValue, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName, // name do input nativo
      ref: inputRef.current, // referencia para acessar ele do elemento pai
      getValue(refs) {
        const checked = refs.find((ref: any) => ref.checked);

        return checked ? checked.value : null;
      },
      setValue(refs, value) {
        const item = refs.find((ref: any) => ref.value === value);

        if (item) {
          item.checked = true;
        }
      },
    });
  }, [fieldName, registerField]);

  const changeRef = useCallback((ref, index) => {
    if (inputRef.current) {
      inputRef.current[index] = ref;
    }
  }, []);

  const clickFunction = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      // if (clickExtraFunction) {
      //   const checked = inputRef.current.find((ref: any) => ref.checked);
      //   if (checked) {
      //     clickExtraFunction(checked.value);
      //   }
      // }

      setValueInput((event.target as HTMLInputElement).value);
    },
    [clickExtraFunction]
  );

  //   const styles = theme => ({
  //   radio: {
  //     '&$checked': {
  //       color: '#4B8DF8'
  //     }
  //   },
  //   checked: {}
  // })

  return (
    <Container orientation={orientation}>
      <RadioGroup
        aria-label="gender"
        name="gender1"
        value={valueInput}
        onChange={clickFunction}
        defaultValue="2"
      >
        {options.map((option, index) => (
          <FormControlLabel
            value={String(option.id)}
            control={<Radio classes={{ root: "test" }} />}
            label={option.label}
            classes={{ root: "test" }}
          />
        ))}
      </RadioGroup>

      {/* {options.map((option, index) => (
        <Radio
          // checked={selectedValue === 'a'}
          onChange={clickFunction}
          value={option.id}
          name={fieldName}
          inputProps={{ "aria-label": "A" }}
          classes={{ root: "teste" }}
          // withStyles({
          //   root: {
          //     color: green[400],
          //     '&$checked': {
          //       color: green[600],
          //     },
          //   }})
        >
          {option.label}
        </Radio>
      ))} */}
    </Container>
  );
};

export default RadioButton;
