import styled from "styled-components";

interface PropsSmaller {
  small?: boolean;
}

export const Container = styled.div<PropsSmaller>`
  width: ${(props) => (props.small ? "260px" : "325px")};
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
`;

export const Box = styled.div<PropsSmaller>`
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  border: 0;
  display: flex;
  position: relative;
  font-size: 0.875rem;
  min-width: 0;
  word-wrap: break-word;
  background: #fff;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
  margin-top: 30px;
  border-radius: 6px;
  margin-bottom: 30px;
  flex-direction: column;
  height: ${(props) => (props.small ? "auto" : "135px")};
`;

interface IconBoxProps {
  color: string;
  small?: boolean;
}
export const IconBox = styled.div<IconBoxProps>`
  width: ${(props) => (props.small ? "70px" : "86px")};
  height: ${(props) => (props.small ? "70px" : "87px")};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.color};
  box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.14),
    0 7px 10px -5px rgba(255, 152, 0, 0.4);
  border-radius: 3px;
  position: absolute;
  margin-top: ${(props) => (props.small ? "-25px" : "-25px")};
  margin-left: ${(props) => (props.small ? "10px" : "20px")};

  svg {
    fill: #ffffff;
  }

  span {
    font-size: ${(props) => (props.small ? "10px" : "14px")};
    position: relative;
    margin-top: 10px;
    color: #ffffff;
  }
`;

interface InfoProps {
  unity?: boolean;
  small?: boolean;
}

export const Info = styled.div<InfoProps>`
  margin-left: auto;
  width: ${(props) => (props.small ? "188px" : "219px")};
  display: flex;
  flex-direction: ${(props) => (props.unity ? "row" : "column")};
  align-items: center;
  justify-content: center;
  margin-top: ${(props) => (props.small ? "38px" : "30px")};
`;

export const Title = styled.span<PropsSmaller>`
  font-weight: bold;
  font-size: ${(props) => (props.small ? "13px" : "20px")};
  margin-bottom: 9px;
`;

export const Icon = styled.img<PropsSmaller>`
  width: ${(props) => (props.small ? "30px" : "40px")};
  height: ${(props) => (props.small ? "30px" : "40px")};
`;

interface LegendsProps {
  unity?: boolean;
}
export const Legends = styled.div<LegendsProps>`
  margin: ${(props) =>
    props.unity ? "16px 10px 10px 10px" : "31px 10px 10px 10px"};
  border-top: solid 1px #eee;
  padding-top: 10px;

  span {
    margin-top: 20px;
    margin-left: 10px;
    color: #999;
  }
`;

export const BoxInner = styled.div`
  display: flex;
  align-items: center;
`;
