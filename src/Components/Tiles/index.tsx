import React, { FC, ReactNode } from "react";
import {
  Container,
  Box,
  IconBox,
  Info,
  Title,
  Icon,
  Legends,
  BoxInner,
} from "./styles";
import VolumeIcon from "../../Assets/volume.svg";
import Input from "../../Components/Input";
import RadioButton from "../../Components/RadioButton";

interface Props {
  title?: string;
  color: string;
  legend?: string;
  icon: any;
  small?: boolean;
  unity?: boolean;
  horizontal?: boolean;
  styleContainer?: object;
  styleInfo?: object;
  styleBox?: object;
  styleLegends?: object;
  defaultValue?: number | string;
}

const Tiles: FC<Props> = ({
  title,
  color,
  legend,
  icon,
  small,
  unity,
  horizontal,
  styleContainer = {},
  styleInfo = {},
  styleBox = {},
  styleLegends = {},
  defaultValue,
}) => (
  <Container small={small} style={styleContainer}>
    <Box small={small} style={styleBox}>
      <IconBox color={color} small={small}>
        <Icon src={icon || VolumeIcon} color="#FFFFFF" small={small} />
        {!small && <span>{legend}</span>}
      </IconBox>

      <Info small={small} style={styleInfo}>
        <Title small={small}>{title}</Title>

        {unity && horizontal && (
          <BoxInner>
            <RadioButton
              name="unity"
              orientation="vertical"
              options={[
                { id: 1, label: "Barom. Pressure" },
                { id: 2, label: "Altitude" },
              ]}
            />

            <Input
              name="volume"
              borderColor="#999999"
              small={small}
              defaultValue={defaultValue}
            />
          </BoxInner>
        )}
        {unity && !horizontal && (
          <RadioButton
            name="unity"
            orientation="vertical"
            options={[
              { id: 1, label: "English Units" },
              { id: 2, label: "Metrics Units" },
            ]}
            defaultValue={1}
          />
        )}

        {!unity && !horizontal && (
          <Input
            name="volume"
            borderColor="#999999"
            small={small}
            style={{ marginTop: 16 }}
            defaultValue={defaultValue}
          />
        )}
      </Info>

      {small && (
        <Legends style={styleLegends} unity={unity}>
          <span>{legend}</span>
        </Legends>
      )}
    </Box>
  </Container>
);

export default Tiles;
