import React, { FC, useState, useCallback, useEffect } from "react";
import LogoFan from "../../Assets/logo2.png";

import { Container, Content, Logo, Barrinha, MenuItem } from "./styles";
import { GiComputerFan } from "react-icons/gi";
import { BsNewspaper } from "react-icons/bs";
import { HiOutlineInformationCircle } from "react-icons/hi";
import { AiOutlinePrinter } from "react-icons/ai";
import { RiLogoutBoxLine } from "react-icons/ri";
import Button from "@material-ui/core/Button";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import DrawerAbout from "../../Components/DrawerAbout";

interface ItemProps {
  id: number;
  label: string;
  icon: React.ReactNode;
  link: string;
}

const Menu: FC = () => {
  const [visibleDrawer, setVisibleDrawer] = useState(false);
  const location = useLocation();
  const parsed = queryString.parse(location.search);
  const [selected, setSelected] = useState<any>(parsed.menu || "1");
  const history = useHistory();

  const items = [
    {
      id: 1,
      label: "Fan Type",
      icon: <GiComputerFan size={20} color="#FFFFFF" />,
      link: "/?menu=1",
    },
    {
      id: 2,
      label: "Dashboard",
      icon: <BsNewspaper size={20} color="#FFFFFF" />,
      link: "/rating?menu=2",
    },
    {
      id: 3,
      label: "Information",
      icon: <HiOutlineInformationCircle size={20} color="#FFFFFF" />,
      link: "/rating?menu=3",
      action: () => setVisibleDrawer(true),
    },
    {
      id: 4,
      label: "Print",
      icon: <AiOutlinePrinter size={20} color="#FFFFFF" />,
      link: "",
    },
    {
      id: 5,
      label: "Exit",
      icon: <RiLogoutBoxLine size={20} color="#FFFFFF" />,
      link: "/rating?menu=5",
    },
  ];

  const handleClick = useCallback((item: ItemProps, action: () => void) => {
    setSelected(String(item.id));

    setTimeout(() => {
      action ? action() : history.push(item.link);
    }, 300);
  }, []);

  useEffect(() => {
    console.log("parsed: ", parsed);

    setSelected(parsed.menu || "1");
  }, []);

  const handleCloseDrawer = useCallback(() => {
    setVisibleDrawer(false);
  }, []);

  return (
    <Container>
      <DrawerAbout visible={visibleDrawer} handleClose={handleCloseDrawer} />
      <Content>
        <Logo src={LogoFan} />
        <Barrinha />

        {items.map((i) => (
          <Button
            key={i.id}
            startIcon={i.icon}
            className={String(i.id) === selected ? "button-active" : ""}
            onClick={() => handleClick(i, i.action)}
          >
            {i.label}
          </Button>
        ))}
      </Content>
    </Container>
  );
};

export default Menu;
