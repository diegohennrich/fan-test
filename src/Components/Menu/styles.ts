import styled from "styled-components";
import Bg from "../../Assets/bg1.jpeg";

export const Container = styled.div`
  width: 260px;
  min-height: 100vh;
  height: auto;
  background: black;
  position: relative;
  background-image: url(${Bg});
  /* background-repeat: no-repeat; */
  display: flex;
`;

export const Content = styled.div`
  width: 260px;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  position: absolute;
  top: 0;
  left: 0;

  display: flex;
  flex-direction: column;
  align-items: center;

  .button-active {
    background: #0475b9;
    transition: all 300ms linear;
  }

  .button-active:hover {
    background: #0475b9;
  }

  button {
    color: white;
    width: 90%;
    margin: 5px 15px;
    transition: all 300ms linear;
    display: flex;
    justify-content: flex-start;
    padding-left: 40px;

    span {
      color: #fff;

      font-size: 14px;
      font-family: "Roboto", "Helvetica", "Arial", sans-serif;
      font-weight: 300;
      line-height: 30px;
    }
  }
`;

export const Logo = styled.img`
  width: 100px;
  margin-top: 50px;
`;

export const Barrinha = styled.div`
  height: 1px;
  width: 90%;
  background: rgba(255, 255, 255, 0.4);
  margin: 0 15px;
  margin-top: 15px;
  margin-bottom: 10px;
`;

export const MenuItem = styled.div`
  display: flex;
  align-items: center;
  height: 56px;
  width: 100%;

  svg {
    margin-left: 10px;
  }

  span {
    color: #fff;
    margin-left: 15px;
    font-size: 14px;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    font-weight: 300;
    line-height: 30px;
  }
`;
