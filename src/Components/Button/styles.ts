import styled from "styled-components";
import { shade } from "polished";
import Colors from "../../Utils/Colors";

interface ButtonProps {
  bgColor?: string;
  color?: string;
  size?: string;
  padding?: string;
  height?: string;
}
export const Container = styled.button<ButtonProps>`
  width: ${(props) => props.size || "100%"};
  background: ${(props) => props.bgColor || Colors.backgroundSecundary};
  color: ${(props) => props.color || Colors.primary};
  /* height: 56px; */
  padding: 10px 25px;
  border-radius: 10px;
  border: 0px;
  font-weight: 500;
  height: ${(props) => props.height || "auto"};
  transition: background-color 0.2s;
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: center;
  position: relative;
  outline: none !important;
  font-family: Helvetica Neue;

  svg {
    margin-right: 15px;
  }

  &:hover {
    background: ${shade(0.2, Colors.backgroundSecundary)};
  }
`;
