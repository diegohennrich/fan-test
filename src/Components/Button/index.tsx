import React, { FC, ButtonHTMLAttributes, ComponentType } from "react";
import { IconBaseProps } from "react-icons";
import { Container } from "./styles";

// type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;
interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  bgColor?: string;
  color?: string;
  size?: string;
  icon?: ComponentType<IconBaseProps>;
  height?: string;
}
const Button: FC<ButtonProps> = ({
  children,
  bgColor,
  color,
  height,
  icon: Icon,
  ...rest
}) => {
  return (
    <>
      <Container
        height={height}
        bgColor={bgColor}
        color={color}
        type="button"
        {...rest}
      >
        {Icon && <Icon size={20} color="#FFFFFF" />}
        {children}
      </Container>
    </>
  );
};

export default Button;
