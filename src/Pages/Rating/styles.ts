import styled from "styled-components";

export const Container = styled.div`
  background: #eeeeee;
  display: flex;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  width: 100%;

  .btn-calculate {
    width: 250px;
    height: 50px;
    margin-top: 10px;
    background: #0475b9;
  }
`;

export const Content = styled.div`
  /* padding: 20px; */
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

export const Header = styled.div`
  border-radius: 6px;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
  background: #ffffff;
  height: 46px;
  width: 100%;
  display: flex;
  align-items: center;
  padding-left: 10px;
  font-weight: 400;
  margin-bottom: 20px;
`;

export const BoxTiles = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const Alinhador = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
  padding: 20px;
`;

export const AlignContent = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

export const Box = styled.div`
  .active {
    transform: rotate(-90deg);
  }
`;

export const IconRight = styled.div`
  margin-left: auto;
  margin-right: 20px;
  cursor: pointer;
  width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: 0.2s;
`;

export const ModelSelected = styled.span`
  width: 200px !important;
  margin-left: auto;
  margin-right: 20px;
  height: 40px;
  border-radius: 6px;
  background: linear-gradient(60deg, #26c6da, #00acc1);
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const BoxAlign = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
