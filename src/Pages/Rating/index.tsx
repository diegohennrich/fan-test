import React, { FC, useRef, useState, useCallback } from "react";
import { FormHandles } from "@unform/core";
import { Form } from "@unform/web";
import {
  Container,
  Content,
  Header,
  BoxTiles,
  Alinhador,
  AlignContent,
  Box,
  IconRight,
  ModelSelected,
  BoxAlign,
} from "./styles";
import Menu from "../../Components/Menu";
import Tiles from "../../Components/Tiles";
import VolumeIcon from "../../Assets/volume.svg";
import PressureIcon from "../../Assets/pressure.svg";
import DensityIcon from "../../Assets/density.svg";
import AirIcon from "../../Assets/air-temperature.svg";
import ManometerIcon from "../../Assets/manometer.svg";
import InletPressureIcon from "../../Assets/inlet-pressure.svg";
import MoleculeIcon from "../../Assets/molecule.svg";
import { Button, Tooltip } from "@material-ui/core";
import Table from "../../Components/Table";
import Drawer from "../../Components/Drawer";
import Card from "../../Components/Card";
import Modelil from "../../Assets/models/il.png";
import { FaChevronLeft } from "react-icons/fa";
import DataPrincipal from "./data/principal";

const Rating: FC = () => {
  const formRef = useRef<FormHandles>(null);
  const [tableVisible, setTableVisible] = useState(false);
  const [visibleDrawer, setVisibleDrawer] = useState(false);
  const [activeAdvanced, setActiveAdvanced] = useState(false);

  const handleCloseDrawer = useCallback(() => {
    setVisibleDrawer(false);
  }, []);

  const handleClickCheckbox = useCallback(() => {
    setVisibleDrawer(true);
  }, []);

  const handleClickChevron = useCallback(() => {
    setActiveAdvanced((prev) => !prev);
  }, []);

  return (
    <Container>
      <Drawer visible={visibleDrawer} handleClose={handleCloseDrawer} />
      <Menu />
      <Alinhador>
        <Form ref={formRef} onSubmit={() => {}} className="form-home">
          <Content>
            <Header>
              <span>Fan Rating:</span>
            </Header>
            <BoxTiles>
              <Tiles
                title="VOLUME"
                color="linear-gradient(60deg, #ffa726, #fb8c00);"
                legend="Cu Ft/Min"
                icon={VolumeIcon}
                small
                defaultValue={1000}
              />

              <Tiles
                title="SP"
                color="linear-gradient(60deg, #66bb6a, #43a047)"
                legend="Inches W. G."
                icon={PressureIcon}
                small
                defaultValue={500}
              />

              <Tiles
                title="DENSITY"
                color="linear-gradient(60deg, #ef5350, #e53935)"
                legend="Lb / Cu Ft"
                icon={DensityIcon}
                small
                defaultValue={90}
              />

              <Tiles
                title="UNIT"
                color="linear-gradient(60deg, #26c6da, #00acc1)"
                small
                icon={DensityIcon}
                unity
              />

              <BoxAlign>
                <Card
                  title="Model Il"
                  image={Modelil}
                  justImage={true}
                  styleTitle={{ fontSize: 14 }}
                  styleImage={{ height: 50 }}
                  styleInfo={{ height: 50 }}
                  styleContainer={{ marginBottom: 0, maxWidth: 250 }}
                />

                <Button
                  classes={{ root: "btn-calculate" }}
                  variant="contained"
                  color="primary"
                  onClick={() => setTableVisible(true)}
                >
                  CALCULATE
                </Button>
              </BoxAlign>
            </BoxTiles>

            <AlignContent>
              <Box style={{ width: "100%" }}>
                <Header>
                  <span>Advanced Density Inputs</span>
                  <IconRight
                    className={activeAdvanced ? "active" : ""}
                    onClick={handleClickChevron}
                  >
                    <FaChevronLeft />
                  </IconRight>
                </Header>
                {activeAdvanced && (
                  <BoxTiles>
                    <Tiles
                      title="AIR TEMPERATURE"
                      color="linear-gradient(326deg, #a4508b 0%, #5f0a87 74%)"
                      legend="Degrees F"
                      icon={AirIcon}
                      small
                      defaultValue={70}
                    />

                    <Tiles
                      color="linear-gradient(326deg, #a4508b 0%, #5f0a87 74%)"
                      legend="Feet"
                      icon={ManometerIcon}
                      small
                      unity
                      horizontal
                      styleContainer={{ width: 450 }}
                      styleLegends={{ marginTop: 27 }}
                      styleInfo={{ width: 360 }}
                      defaultValue={0}
                    />

                    <Tiles
                      title="INLET SP"
                      color="linear-gradient(326deg, #a4508b 0%, #5f0a87 74%)"
                      legend="W.G"
                      icon={InletPressureIcon}
                      small
                      defaultValue={0}
                    />

                    <Tiles
                      title="MOLECULAR WEIGHT"
                      color="linear-gradient(326deg, #a4508b 0%, #5f0a87 74%)"
                      small
                      icon={MoleculeIcon}
                      defaultValue={28.965}
                    />
                  </BoxTiles>
                )}
              </Box>
            </AlignContent>
          </Content>
        </Form>

        {tableVisible && (
          <Table
            headers={DataPrincipal.headers}
            data={DataPrincipal.data}
            title="FAN LIST RESULT"
            handleClick={handleClickCheckbox}
          />
        )}
      </Alinhador>
    </Container>
  );
};

export default Rating;
