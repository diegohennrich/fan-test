export default {
  headers: ["63", "125", "250", "500", "1000", "2000", "4000", "8000"],
  data: [
    {
      title: "PWL Fan",
      id: 1,
      size: 89,
      diameter: 103,
      rpm: 100,
      bhp: 102,
      staff: 99,
      speed: 93,
      velocity: 85,
      other: 77,
    },
  ],
};
