import React, { FC, useRef } from "react";
import { Container, Alinhador } from "./styles";
import Menu from "../../Components/Menu";
import Card from "../../Components/Card";
import Modelil from "../../Assets/models/il.png";
import ModelWb from "../../Assets/models/wb.png";
import Modelcv from "../../Assets/models/cv.png";
import Modeltahd from "../../Assets/models/tahd.png";
import Modelrb from "../../Assets/models/rb.png";
import Modeltaub from "../../Assets/models/taub.png";
import Modelbc from "../../Assets/models/bc.png";
import Modeltah from "../../Assets/models/tah.png";

const Type: FC = () => {
  const data = [
    {
      id: 1,
      title: "MODEL IL",
      description: "INLINE CENTRIFUGAL",
      image: Modelil,
    },
    {
      id: 2,
      title: "MODEL WB",
      description: "WALL MOUNTED FAN",
      image: ModelWb,
    },
    {
      id: 3,
      title: "MODEL CV",
      description: "CORRO CENTRIFUGAL",
      image: Modelcv,
    },
    {
      id: 4,
      title: "MODEL TAHD",
      description: "TUBE AXIAL UPBLAST",
      image: Modeltahd,
    },
    {
      id: 5,
      title: "MODEL RB",
      description: "RADIAL CENTRIFUGAL UTILITY FAN",
      image: Modelrb,
    },
    {
      id: 6,
      title: "MODEL TAUB",
      description: "TUBE AXIAL UPBLAST",
      image: Modeltaub,
    },
    {
      id: 7,
      title: "MODEL BC",
      description: "BACKWARD CURVED CENTRIFUGAL UTILITY FAN",
      image: Modelbc,
    },
    {
      id: 8,
      title: "MODEL TAH",
      description: "TUBE AXIAL HIGH PRESSURE",
      image: Modeltah,
    },
    {
      id: 9,
      title: "MODEL TAL",
      description: "TUBE AXIAL LOW PRESSURE",
      image: Modeltah,
    },
  ];

  return (
    <Container>
      <Menu />
      <Alinhador>
        {data.map((i) => (
          <Card
            key={i.id}
            image={i.image}
            title={i.title}
            description={i.description}
            styleContainer={{ maxWidth: 250, maxHeight: 380, margin: 10 }}
            styleImage={{ height: 120 }}
            styleInfo={{ height: 210 }}
            styleTitle={{ fontSize: 18, marginTop: 20 }}
            styleDescription={{ fontSize: 13, marginTop: 20 }}
            styleBtn={{ width: 200 }}
          />
        ))}
      </Alinhador>
    </Container>
  );
};

export default Type;
