import styled from "styled-components";

export const Container = styled.div`
  background: #eeeeee;
  display: flex;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  width: 100%;

  .btn-calculate {
    width: 250px;
    height: 50px;
    margin-top: 70px;
    background: #0475b9;
  }

  .form-home {
    /* display: flex;
    flex: 1;
    align-items: flex-start; */
  }
`;

export const Alinhador = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  padding: 20px;
`;
