import React, { FC, useRef } from "react";
import { FormHandles } from "@unform/core";
import { Form } from "@unform/web";
import {
  Container,
  ContentForm,
  Box,
  RightActions,
  Inputs,
  Units,
  Item,
  Image,
  TitleItem,
  OtherButtons,
  Header,
  Actions,
  ButtonHeader,
  TitleBox,
  Ball,
  Info,
  BoxItem,
} from "./styles";
import {
  FiLogOut,
  FiBarChart,
  FiMaximize,
  FiHelpCirclev,
  FiHelpCircle,
} from "react-icons/fi";
import { CgCompress, CgCalculator, CgNotes } from "react-icons/cg";
import { RiInformationLine } from "react-icons/ri";
import { GiComputerFan } from "react-icons/gi";

import {
  AiOutlineLineChart,
  AiOutlinePrinter,
  AiOutlineArrowRight,
} from "react-icons/ai";
import Input from "../../Components/Input";
import RadioButton from "../../Components/RadioButton";
import Button from "../../Components/Button";

const Home: FC = () => {
  const formRef = useRef<FormHandles>(null);
  const models = [
    "TA-LP",
    "TA-HP",
    "UB-TA",
    "TA-V",
    "Select BIC",
    "Select RBC",
  ];

  const notes = [
    "Power rating(BHP) does not include drive losses",
    "Diameter in Inches",
    "Tip Speed in FPM",
    "Outlet Velocity in FPM",
    "Performance shown is for installation type B and D - Free or Ducted inlet, Ducted outlet",
    "Performance ratings do not include the effects of appurtenances in the airstream",
  ];
  return (
    <>
      <Header>
        <Actions>
          <ButtonHeader>
            <CgNotes color="#FFFFFF" />
            <span>Notes</span>
          </ButtonHeader>
          <ButtonHeader>
            <AiOutlinePrinter color="#FFFFFF" />
            <span>Print</span>
          </ButtonHeader>
          <ButtonHeader>
            <RiInformationLine color="#FFFFFF" />
            <span>Information</span>
          </ButtonHeader>

          <ButtonHeader>
            <FiLogOut color="#FFFFFF" />
            <span>Exit</span>
          </ButtonHeader>
        </Actions>
      </Header>
      <Container>
        <Box>
          <ContentForm>
            <Form
              ref={formRef}
              onSubmit={() => {}}
              style={{ width: "100%", padding: 10 }}
            >
              <TitleBox>
                <Ball /> <h1>Fan Rating</h1>
              </TitleBox>

              <Inputs>
                <Input
                  name="test"
                  type="text"
                  icon={FiBarChart}
                  placeholder="Volume"
                  legend="Cu Ft/Min"
                />

                <Input
                  name="test"
                  type="text"
                  icon={CgCompress}
                  placeholder="SP"
                  legend="Inches W.G."
                />

                <Input
                  name="test"
                  type="text"
                  icon={FiMaximize}
                  placeholder="Density"
                  legend="Lb / Cu Ft"
                />

                <Button
                  bgColor="#0190fe"
                  color="#FFFFFF"
                  type="submit"
                  icon={CgCalculator}
                  size="200px"
                  height="56px"
                >
                  Calculate
                </Button>

                <Units>
                  <RadioButton
                    name="cota"
                    clickExtraFunction={() => {}}
                    options={[
                      {
                        id: "english",
                        label: "English Units",
                      },
                      {
                        id: "metric",
                        label: "Metric Units",
                      },
                    ]}
                  />
                </Units>
              </Inputs>
            </Form>
          </ContentForm>

          <RightActions>
            {models.map((i) => (
              <Item>
                <Image
                  src={
                    "https://image.made-in-china.com/43f34j00rJlanWztaYbo/Malaysia-Industrial-Fan-Wall-Mounted-Exhaust-Fan.jpg"
                  }
                />

                <Button
                  bgColor="#0190fe"
                  color="#FFFFFF"
                  type="submit"
                  height="56px"
                  icon={GiComputerFan}
                  style={{ marginTop: 20 }}
                >
                  {i}
                </Button>
              </Item>
            ))}

            <OtherButtons>
              <Button
                bgColor="#0190fe"
                color="#FFFFFF"
                type="submit"
                height="56px"
                size="360px"
                icon={AiOutlineLineChart}
                style={{ marginTop: 5 }}
              >
                Curve
              </Button>

              <Info>
                <div>
                  <FiHelpCircle color="#FFFFFF" size={25} />
                  <h1>Notes</h1>
                </div>

                {notes.map((i) => (
                  <BoxItem>
                    <AiOutlineArrowRight
                      color="#FFFFFF"
                      size={20}
                      style={{ display: "block" }}
                    />
                    <span>{i}</span>
                  </BoxItem>
                ))}
              </Info>
            </OtherButtons>
          </RightActions>
        </Box>
      </Container>
    </>
  );
};

export default Home;
