import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  font-family: Helvetica Neue;
  padding: 0 50px;
`;

export const ContentForm = styled.div`
  display: flex;
  border-radius: 10px;
  border: solid 1px black;
  height: 200px;
  width: 100%;
`;

export const Box = styled.div`
  display: flex;
  width: 100%;
  margin-top: 50px;

  h1 {
    font-size: 16px;
  }
`;

export const RightActions = styled.div`
  display: flex;
  flex-direction: row;
  width: 600px;
  flex-wrap: wrap;
  margin-left: 10px;
`;

export const Inputs = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
`;

export const Units = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5px;
  margin-right: 15px;
`;

export const Item = styled.div`
  border-radius: 10px;
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  min-width: 150px;

  border: solid 1px #0190fe;
  margin-bottom: 10px;
  margin-right: 10px;
`;

export const Image = styled.img`
  width: 100px;
`;

export const TitleItem = styled.span`
  font-family: Helvetica Neue;
`;

export const OtherButtons = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  width: 100%;
  background: #0190fe;
  height: 56px;

  display: flex;
  align-items: center;
`;

export const Actions = styled.div`
  display: flex;
  margin-left: auto;
`;

export const ButtonHeader = styled.div`
  display: flex;
  flex-direction: row;
  cursor: pointer;
  margin-right: 20px;

  span {
    font-family: Helvetica Neue;
    color: #ffffff;
    margin-left: 10px;
  }
`;

export const TitleBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: auto;
  margin-bottom: 20px;
`;

export const Ball = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background: #ff9000;
  margin-right: 5px;
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  background: #ff9000;
  padding: 10px;
  margin-top: 10px;
  margin-bottom: 30px;
  width: 340px;

  color: white;
  font-family: Helvetica Neue;

  span {
    margin-bottom: 10px;
  }

  > div {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-bottom: 15px;

    svg {
      margin-right: 10px;
    }
  }
`;

export const BoxItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 15px;

  span {
    margin: 0;
  }

  svg {
    margin-right: 10px;
  }
`;
