import { hot } from "react-hot-loader";
import * as React from "react";
import { HashRouter } from "react-router-dom";
import Routes from "./Routes";
import { FC } from "react";

const App: FC = () => (
  <>
    <HashRouter>
      <Routes />
    </HashRouter>
  </>
);

export default hot(module)(App);
