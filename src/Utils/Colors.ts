export default {
  backgroundPrimary: '#d5dae5',
  backgroundSecundary: '#f5f5fb',
  primary: '#000000',
  primaryFont: '#FFFFFF',
  gray: '#b1b6d1',
  purple: '#5e81f4',
  blue: '#4c4cff',
  white: '#FFFFFF',
  inputs: {
    dark: {
      bgColor: '#232129',
      color: '#f4ede8',
    },
    light: {
      bgColor: '#FFFFFF',
      color: '#232129',
    },
  },
};
