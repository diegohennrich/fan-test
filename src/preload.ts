import { IpcRenderer } from "electron";
export {};
declare global {
  interface Window {
    ipcRenderer: IpcRenderer;
  }
}

window.ipcRenderer = require("electron").ipcRenderer;
